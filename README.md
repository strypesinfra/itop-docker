# itop-docker

Relatively minimal docker image for iTop. It uses webdevops/php-apache:alpine-php7 as base, and does not include database (but should be linked to one, running in separate docker container for example).